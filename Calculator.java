package com.deveducation.www.petprojects;

import javax.swing.*;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTextField;

public class Calculator {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
    }
}

class MyFrame extends JFrame implements ActionListener {
    final private Container c;
    final private JLabel label1, label2, label3, label4;
    final private JTextField text1, text2, text3, text4;
    final private JButton equally;


    MyFrame() {

        setTitle("Calculator");
        setSize(400, 400);
        setLocation(100, 100);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        c = getContentPane();
        c.setLayout(null);

        label1 = new JLabel("First number: ");
        label1.setBounds(10, 17, 100, 20);
        c.add(label1);

        text1 = new JTextField();
        text1.setBounds(120, 20, 200, 20);
        c.add(text1);

        label2 = new JLabel("Math operator: ");
        label2.setBounds(10, 60, 100, 20);
        c.add(label2);

        text2 = new JTextField();
        text2.setBounds(120, 62, 200, 20);
        c.add(text2);

        label3 = new JLabel("Second number: ");
        label3.setBounds(10, 100, 100, 20);
        c.add(label3);

        text3 = new JTextField();
        text3.setBounds(120, 102, 200, 20);
        c.add(text3);

        equally = new JButton("=");
        equally.setBounds(183, 147, 70, 30);
        c.add(equally);


        label4 = new JLabel("The result: ");
        label4.setBounds(10, 200, 100, 20);
        c.add(label4);

        text4 = new JTextField();
        text4.setBounds(120, 205, 200, 20);
        c.add(text4);

        equally.addActionListener(this);
        setVisible(true);

    }

    public void actionPerformed(ActionEvent event) {
        try {
            if (text2.getText().equals("+") || text2.getText().equals("-") || text2.getText().equals("*") || text2.getText().equals("/")) {
                if (text2.getText().equals("+")) {
                    double a = Double.parseDouble(text1.getText());
                    double b = Double.parseDouble(text3.getText());
                    double c = a + b;
                    text4.setText(Double.toString(c));
                }
                if (text2.getText().equals("-")) {
                    double a = Double.parseDouble(text1.getText());
                    double b = Double.parseDouble(text3.getText());
                    double c = a - b;
                    text4.setText(Double.toString(c));
                }
                if (text2.getText().equals("*")) {
                    double a = Double.parseDouble(text1.getText());
                    double b = Double.parseDouble(text3.getText());
                    double c = a * b;
                    text4.setText(Double.toString(c));
                }

                if (text2.getText().equals("/")) {
                    double a = Double.parseDouble(text1.getText());
                    double b = Double.parseDouble(text3.getText());
                    double c = a / b;
                    text4.setText(Double.toString(c));
                }

            } else {
                text4.setText("Please input math operator (+, -, *, /)");
            }
        } catch (NumberFormatException e1) {
            text4.setText("Please input digit");
        } catch (ArithmeticException e1) {
            text4.setText("Division by zero");
        }
        text1.setText("");
        text2.setText("");
        text3.setText("");
    }
}




